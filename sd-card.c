#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "inc/hw_memmap.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "utils/cmdline.h"
#include "utils/uartstdio.h"
#include "driverlib/fpu.h"
#include "ff.h"
#include "diskio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "driverlib/ssi.h"


int32_t i32DataCount = 0;


static FATFS g_sFatFs;
static FIL g_sFileObject;
WORD buffer[4096];
FRESULT rc;
UINT br, bw;

void SysTickHandler(void)
{
    disk_timerproc();
}

// function to initialize onboard UART.
void uart_init(void)
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	UARTStdioConfig(0, 115200, SysCtlClockGet());
}

// main function
int main(void)
{
    uint32_t ui32Data;

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);


	// configure the systick timer for a 100Hz interrupt. required by the FatFs driver.
    SysTickPeriodSet(ROM_SysCtlClockGet() / 100);
    SysTickIntRegister(SysTickHandler);
    SysTickEnable();
    SysTickIntEnable();

    // enable interrupts
    IntMasterEnable();

    //  initialize UART for console I/O
    uart_init();

    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE,GPIO_PIN_5|GPIO_PIN_3|GPIO_PIN_2);

    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_PIN_4);

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_1, SSI_MODE_MASTER, 10000, 16);
    SSIEnable(SSI0_BASE);

    //  mount the file system, using logical disk 0.
    rc = f_mount(0, &g_sFatFs);
    if(rc != FR_OK)
    {
        UARTprintf("f_mount error\n");
        while(1){}
    }
    else
    	UARTprintf("f_mount success!\n");

    rc = f_open(&g_sFileObject, "data1.csv", FA_READ);
    if(rc != FR_OK)
    {
        UARTprintf("f_open error %d\n", rc);
        while(1){}
    }
    else
        UARTprintf("new file created\n");



    //
    //  leitura do arquivo
    //

    int16_t valor,valor1;
    int i = 0;
    while(1)
    {
        valor1=0;
        valor = 0;
        f_read(&g_sFileObject, buffer, 1, &br);
        if(!br)
        {
            f_lseek(&g_sFileObject, 0);
            f_read(&g_sFileObject, buffer, 1, &br);
        }
        while(buffer[0] != ',')
        {
            valor = valor*10 + buffer[0]-48;
            f_read(&g_sFileObject, buffer, 1, &br);
            if(!br)
            {
                f_lseek(&g_sFileObject, 0);
                f_read(&g_sFileObject, buffer, 1, &br);
                valor1 = 1;
                break;
            }
        }

        if(valor1 == 0)
        {
            while(buffer[0] != ',')
            {
                valor = valor*10 + buffer[0]-48;
                f_read(&g_sFileObject, buffer, 1, &br);
            }
        }
        ui32Data = valor;
        GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0x00);
        SSIDataPut(SSI0_BASE, ui32Data);
        while(SSIBusy(SSI0_BASE))
        {
        }
        GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_PIN_4);
        i++;
    }

}

